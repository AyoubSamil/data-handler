package com.pi.dataHandler.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
public class CsvHandlerService {

	@Autowired
	private StorageFile storageFile;

	@Value("#{'${data_out}'}")
	private String dataOutPath;

	@Value("#{'${header}'.split(',')}")
	private String[] header;

	public List<String[]> data;

	public String[] getHeader() {
		return header;
	}

	// key => value -> value=>key
	private Map<String, String> reflectMap(Map<String, String> in) {
		Map<String, String> out = new HashMap<>();
		in.forEach((k, v) -> {
			if (!v.equals("-1")) {
				out.put(v, k);
			}
		});
		return out;
	}

	public void exportAsCsv(String filePath, Map<String, String> indexColumn, String separator) throws IOException {
		String storePath = storageFile.getCurrentPath() + dataOutPath;
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		File out = new File(storePath + "/" + formatter.format(date) + "_" + Paths.get(filePath).getFileName());
		PrintWriter writer = new PrintWriter(out);
		writer.write(String.join(separator, header) + "\n");
		writer.flush();
		Map<String, String> columnIndex = reflectMap(indexColumn);
		BufferedReader reader = storageFile.readInFile(filePath);
		reader.lines().skip(1).parallel().filter(line -> line != "").map((line) -> {
			String[] lineArr = line.split(separator);
			List<String> standarizedLineArr = new ArrayList<String>();
			for (String column : header) {
				String indexStr = columnIndex.get(column);
				if (indexStr != null) {
					int index = Integer.parseInt(indexStr);
					try {
						standarizedLineArr.add(lineArr[index]);
					} catch (ArrayIndexOutOfBoundsException e) {
						standarizedLineArr.add("");
					}
				} else {
					standarizedLineArr.add("");
				}
			}
			return standarizedLineArr;
		}).peek(standarArr -> {
			String strLine = String.join(separator, standarArr) + "\n";
			writer.write(strLine);
			writer.flush();
		}).count();
		reader.close();
		writer.close();
	}

	public List<String[]> readCsvFile(String fileNamePath, String separator, int MaxLines) throws IOException {
		BufferedReader reader = storageFile.readInFile(fileNamePath);
		List<String[]> res = new ArrayList<String[]>();
		try {
			int i = 0;
			while (reader.ready()) {
				if (MaxLines != -1) {
					if (i < MaxLines) {
						String[] line = reader.readLine().split(separator);
						res.add(line);
						i++;
					} else {
						break;
					}

				} else {
					String[] line = reader.readLine().split(separator);
					res.add(line);
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.data = res;
		return res;
	}

	/*
	 * public List<Integer> columnIndexs(MultipartFile csvFile, String separator) {
	 * List<Integer> indexs = new ArrayList<>(); try { BufferedReader reader = new
	 * BufferedReader(new InputStreamReader(csvFile.getInputStream())); if
	 * (reader.ready()) { String[] line = reader.readLine().split(separator); for
	 * (int i = 0; i < line.length; i++) { indexs.add(i); } } } catch (IOException
	 * e) { e.printStackTrace(); } return indexs; }
	 */

	public Map<String, Integer> detectHeader(String fileNamePath, String separator) throws IOException {
		BufferedReader reader = storageFile.readInFile(fileNamePath);
		Map<String, Integer> headerIndex = new HashMap<>();
		if (reader.ready()) {

			reader.readLine();

			String[] line = reader.readLine().split(separator);

			for (int i = 0; i < line.length; i++) {

				String cell = line[i];

				if (isEmail(cell)) {
					headerIndex.put("email", i);
					continue;
				}

				if (isUrl(cell)) {
					headerIndex.put("signup_url", i);
					continue;
				}

				if (isIpAdresse(cell)) {
					headerIndex.put("signup_ip", i);
					continue;
				}

				if (isDateTime(cell)) {
					headerIndex.put("signup_time", i);
					continue;
				}

				if (isGender(cell)) {
					headerIndex.put("gender", i);
					continue;
				}

				if (isPhone(cell)) {
					headerIndex.put("phone1", i);
					continue;
				}
			}
		}
		return headerIndex;
	}

	private boolean isEmail(String cell) {
		return Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$").matcher(cell).matches();
	}

	private boolean isUrl(String cell) {
		return cell.contains("http://") || cell.contains("https://");
	}

	private boolean isDateTime(String cell) {
		String formatedCell = cell.strip().replace("\"", "").replace("T", " ");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		try {
			format.parse(formatedCell);
			return true;
		} catch (ParseException e) {
			return false;
		}

	}

	private boolean isGender(String cell) {
		String cellLowerCase = cell.toLowerCase();
		if (cellLowerCase.equals("f") || cellLowerCase.equals("female") || cellLowerCase.equals("m")
				|| cellLowerCase.equals("male")) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isPhone(String cell) {
		return Pattern.compile("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$").matcher(cell).matches();
	}

	private boolean isIpAdresse(String cell) {
		return Pattern.compile("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$").matcher(cell).matches();
	}
}
