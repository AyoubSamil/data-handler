package com.pi.dataHandler.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface StorageFile {
	
	String getCurrentPath();
	
	List<File> getInFiles() throws IOException;

	BufferedReader readInFile(String fileNamePath) throws IOException;
	
}
