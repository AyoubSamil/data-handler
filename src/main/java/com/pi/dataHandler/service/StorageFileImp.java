package com.pi.dataHandler.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StorageFileImp implements StorageFile {

	@Value("#{'${data_in}'}")
	private String dataInPath;

	@Override
	public List<File> getInFiles() throws IOException {
		List<File> csvFiles = new ArrayList<File>();
		String absoluteDataInPath = getCurrentPath() + dataInPath;
		File dataFolder = new File(absoluteDataInPath);
		if (dataFolder != null && dataFolder.isDirectory()) {
			for (final File fileEntry : dataFolder.listFiles()) {
				if (fileEntry.getName().contains(".csv"))
					csvFiles.add(fileEntry);
			}
			return csvFiles;
		} else {
			throw new RuntimeException("Data Folder Not Found");
		}
	}

	@Override
	public BufferedReader readInFile(String fileNamePath) throws IOException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(new File(fileNamePath))));
	}
	
	public String getCurrentPath() {
		File currentDirFile = new File("");
		String currentDirPathStr = currentDirFile.getAbsolutePath();
		return currentDirPathStr;
	}
	
}
