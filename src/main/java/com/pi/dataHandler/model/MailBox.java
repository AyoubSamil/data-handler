package com.pi.dataHandler.model;

public class MailBox {

	private String email;
	private String email_domain;
	private String signup_ip;
	private String signup_url;
	private String signup_domain;
	private String signup_time;
	private String first_name;
	private String last_name;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String zip_code;
	private String phone1;
	private String phone2;
	private String birth_date;
	private String gender;
	private String occupation;

	public MailBox() {

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail_domain() {
		return email_domain;
	}

	public void setEmail_domain(String email_domain) {
		this.email_domain = email_domain;
	}

	public String getSignup_ip() {
		return signup_ip;
	}

	public void setSignup_ip(String signup_ip) {
		this.signup_ip = signup_ip;
	}

	public String getSignup_url() {
		return signup_url;
	}

	public void setSignup_url(String signup_url) {
		this.signup_url = signup_url;
	}

	public String getSignup_domain() {
		return signup_domain;
	}

	public void setSignup_domain(String signup_domain) {
		this.signup_domain = signup_domain;
	}

	public String getSignup_time() {
		return signup_time;
	}

	public void setSignup_time(String signup_time) {
		this.signup_time = signup_time;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

}
