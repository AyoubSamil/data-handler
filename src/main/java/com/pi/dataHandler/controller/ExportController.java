package com.pi.dataHandler.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller("/test")
public class ExportController {
	
	@PostMapping
	public void export(@RequestBody Object o) {
		System.out.println(o);
	}
}
