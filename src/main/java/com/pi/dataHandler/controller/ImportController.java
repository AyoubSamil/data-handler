package com.pi.dataHandler.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pi.dataHandler.service.CsvHandlerService;
import com.pi.dataHandler.service.StorageFile;

@Controller
public class ImportController {

	@Autowired
	private CsvHandlerService csvHandlerService;

	@Autowired
	private StorageFile storageFile;

	@GetMapping
	public String getAllAvailableFile(Model model) {
		try {
			List<File> files = storageFile.getInFiles();
			model.addAttribute("files", files);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "index";
	}

	@PostMapping(value = "/details")
	public String details(@RequestParam String fileToUploadPath, @RequestParam String separator,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {
		try {
			redirectAttributes.addFlashAttribute("headerList", csvHandlerService.getHeader());
			redirectAttributes.addFlashAttribute("detectedHeader",
					csvHandlerService.detectHeader(fileToUploadPath, separator));
			redirectAttributes.addFlashAttribute("uploadedFile",
					csvHandlerService.readCsvFile(fileToUploadPath, separator, 10));
			redirectAttributes.addFlashAttribute("uploadedFilePath", fileToUploadPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/";
	}

	@PostMapping("/export/out.csv")
	public String export(HttpServletResponse response, @RequestParam Map<String, String> params) {
		String filePath = params.get("uploadedFilePath");
		params.remove("uploadedFilePath");
		try {
			csvHandlerService.exportAsCsv(filePath, params, ";");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/";
	}

	/*
	 * InputStreamResource resource = new InputStreamResource(new
	 * FileInputStream(out));
	 * 
	 * HttpHeaders header = new HttpHeaders();
	 * header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=out.csv");
	 * header.add("Cache-Control", "no-cache, no-store, must-revalidate");
	 * header.add("Pragma", "no-cache"); header.add("Expires", "0");
	 * 
	 * return ResponseEntity.ok().headers(header).contentLength(out.length())
	 * .contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
	 */
}
